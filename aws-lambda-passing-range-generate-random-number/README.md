# Passing two values using url

* update index.js
    ```
console.log('Loading function');

exports.handler = async (event, context, callback) => {
    let min = event.min;
    let max = event.max;
    
    let randomNumber = Math.floor(Math.random() * max) + min;
    
    console.log(' Generated random number is :', randomNumber); // see cloudwatch to check the response on console
    
    // here pass the html code
    
    
   return randomNumber; 
  //callback(null, randomNumber);
};

    ```

* Go to API get method
    * select Integration Request
    * Mapping Template
    * Type application/json
    * pass values
        ```
        console.log('Loading function');

exports.handler = async (event, context, callback) => {
    let min = parseInt(event.min); // sometime you need to cascade it... parseInt()
    let max = parseInt(event.max);
    
    let randomNumber = Math.floor(Math.random() * max) + min;
    
    console.log(' Generated random number is :', randomNumber); // see cloudwatch to check the response on console
    
    // here pass the html code
    
    
   return randomNumber; 
  //callback(null, randomNumber);
};

        ```

> try passing https:yoururl/amazonaws.com/prod?min=50&max=100