# Generate a Random Number from 0 to 6

1. Create a Lambda Function (Configure basic settings)
1. Select inline code on labdma function 
1. Add the index.js file code

```  
console.log('Loading function');
exports.handler = async (event, context) => {
    let min = 0;
    let max = 6;
    
    let randomNumber = Math.floor(Math.random() * max) + min;
    
    console.log(' Generated random number is :', randomNumber); // see cloudwatch to check the response on console
    
    return randomNumber; 
}; 

```

> Try testing - test (No need to pass any argument in json object)

> To see console.log("Hey bud!") messages, see cloudwatch log

* API Gateway
    * Create a resource name
    * Create a method - get
    * Configure this lambda function
    * Deploy
    * Hit the URL