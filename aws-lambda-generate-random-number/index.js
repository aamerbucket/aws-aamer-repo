console.log('Loading function');

exports.handler = async (event, context) => {
    let min = 0;
    let max = 6;
    
    let randomNumber = Math.floor(Math.random() * max) + min;
    
    console.log(' Generated random number is :', randomNumber); // see cloudwatch to check the response on console
    
    return randomNumber; 
};
